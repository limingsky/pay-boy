/*
 * This file is generated by jOOQ.
*/
package com.ndood.merchant.jooq.tables.daos;


import com.ndood.merchant.jooq.tables.OauthAccessToken;
import com.ndood.merchant.jooq.tables.records.OauthAccessTokenRecord;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.8"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class OauthAccessTokenDao extends DAOImpl<OauthAccessTokenRecord, com.ndood.merchant.jooq.tables.pojos.OauthAccessToken, String> {

    /**
     * Create a new OauthAccessTokenDao without any configuration
     */
    public OauthAccessTokenDao() {
        super(OauthAccessToken.OAUTH_ACCESS_TOKEN, com.ndood.merchant.jooq.tables.pojos.OauthAccessToken.class);
    }

    /**
     * Create a new OauthAccessTokenDao with an attached configuration
     */
    public OauthAccessTokenDao(Configuration configuration) {
        super(OauthAccessToken.OAUTH_ACCESS_TOKEN, com.ndood.merchant.jooq.tables.pojos.OauthAccessToken.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getId(com.ndood.merchant.jooq.tables.pojos.OauthAccessToken object) {
        return object.getTokenId();
    }

    /**
     * Fetch records that have <code>create_time IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.OauthAccessToken> fetchByCreateTime(LocalDateTime... values) {
        return fetch(OauthAccessToken.OAUTH_ACCESS_TOKEN.CREATE_TIME, values);
    }

    /**
     * Fetch records that have <code>token_id IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.OauthAccessToken> fetchByTokenId(String... values) {
        return fetch(OauthAccessToken.OAUTH_ACCESS_TOKEN.TOKEN_ID, values);
    }

    /**
     * Fetch a unique record that has <code>token_id = value</code>
     */
    public com.ndood.merchant.jooq.tables.pojos.OauthAccessToken fetchOneByTokenId(String value) {
        return fetchOne(OauthAccessToken.OAUTH_ACCESS_TOKEN.TOKEN_ID, value);
    }

    /**
     * Fetch records that have <code>token IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.OauthAccessToken> fetchByToken(byte[]... values) {
        return fetch(OauthAccessToken.OAUTH_ACCESS_TOKEN.TOKEN, values);
    }

    /**
     * Fetch records that have <code>authentication_id IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.OauthAccessToken> fetchByAuthenticationId(String... values) {
        return fetch(OauthAccessToken.OAUTH_ACCESS_TOKEN.AUTHENTICATION_ID, values);
    }

    /**
     * Fetch records that have <code>user_name IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.OauthAccessToken> fetchByUserName(String... values) {
        return fetch(OauthAccessToken.OAUTH_ACCESS_TOKEN.USER_NAME, values);
    }

    /**
     * Fetch records that have <code>client_id IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.OauthAccessToken> fetchByClientId(String... values) {
        return fetch(OauthAccessToken.OAUTH_ACCESS_TOKEN.CLIENT_ID, values);
    }

    /**
     * Fetch records that have <code>authentication IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.OauthAccessToken> fetchByAuthentication(byte[]... values) {
        return fetch(OauthAccessToken.OAUTH_ACCESS_TOKEN.AUTHENTICATION, values);
    }

    /**
     * Fetch records that have <code>refresh_token IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.OauthAccessToken> fetchByRefreshToken(String... values) {
        return fetch(OauthAccessToken.OAUTH_ACCESS_TOKEN.REFRESH_TOKEN, values);
    }
}
