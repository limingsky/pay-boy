//package com.ndood.web.merchant.service;
//
//import java.util.List;
//
//import javax.annotation.Resource;
//import javax.transaction.Transactional;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.annotation.Rollback;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import com.ndood.merchant.jooq.tables.daos.TJobDao;
//import com.ndood.merchant.jooq.tables.pojos.TJob;
//
///**
// * 
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class TestService {
//
//	@Resource
//    private TJobDao jbDao;
//
//	com.ndood.merchant.jooq.tables.TJob jb =  com.ndood.merchant.jooq.tables.TJob.T_JOB.as("jb");
//    
//    @Test
//    @Rollback
//    @Transactional
//    public void testFind() {
//    	List<TJob> list = jbDao.findAll();
//    	System.out.println(list.get(0));
//    }
//}
