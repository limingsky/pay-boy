package com.ndood.web.merchant.dao;

import java.util.Iterator;
import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.ndood.merchant.jooq.tables.pojos.TJob;

@RunWith(SpringRunner.class)
@SpringBootTest
@SuppressWarnings({ "unchecked", "rawtypes" })
public class TestDao {
	
    @Autowired
    private DSLContext dsl;
    
    com.ndood.merchant.jooq.tables.TJob jb =  com.ndood.merchant.jooq.tables.TJob.T_JOB.as("jb");
    
    /** https://www.programcreek.com/java-api-examples/?api=org.jooq.SQLDialect
     * 删除
     */
    @Test
    public void delete() {
        dsl.delete(jb).where(jb.ID.eq(100));
    }

    /**
     * 增加sxs
     *//*
    @Test
    public void insert() {
    	dslContext.insertInto(table)
		.set(DSL.field("id"), student.getId())
		.set(DSL.field("name"), student.getName())
		.execute();
    	TJob job = new TJob();
        dsl.insertInto(jb).
                columns(jb.BEAN_CLASS,jb.CREATE_BY,jb.CREATE_TIME,jb.CRON_EXPRESSION,
                		jb.DESCRIPTION,jb.IS_CONCURRENT,jb.JOB_GROUP,jb.JOB_NAME,
                		jb.JOB_STATUS,jb.METHOD_NAME,jb.SPRING_BEAN,jb.UPDATE_BY,
                		jb.UPDATE_TIME).
                values(job.getBeanClass(),job.getCreateBy(),job.getCreateTime(),job.getCronExpression(),
                		job.getDescription(),job.getIsConcurrent(),job.getJobGroup(),job.getJobName(),
                		job.getJobStatus(),job.getMethodName(),job.getSpringBean(),job.getUpdateBy(),
                		job.getUpdateTime())
                .execute();
    }

    *//**
     * 更新
     *//*
    @Test
    public void update(com.ndood.merchant.jooq.tables.TJob job) {
        dsl.update(jb).set((Record) job);
    }

    *//**
     * 查询单个
     *//*
    @Test
    public void selectById() {
        Result<?> result =  dsl.select(jb.BEAN_CLASS,jb.CREATE_BY,jb.CREATE_TIME,jb.CRON_EXPRESSION,
        		jb.DESCRIPTION,jb.IS_CONCURRENT,jb.JOB_GROUP,jb.JOB_NAME,
        		jb.JOB_STATUS,jb.METHOD_NAME,jb.SPRING_BEAN,jb.UPDATE_BY,
        		jb.UPDATE_TIME)
                .from(jb)
                .where(jb.ID.eq(1)).fetch();
        System.out.println(new Gson().toJson(result.get(0)));
    }

    /**
     * 查询全部列表
     */
    @Test
	public void selectAll() {
        List<TJob> result = dsl.select().from(jb).fetchInto(TJob.class);
        Iterator<TJob> iterator = result.iterator();
        while(iterator.hasNext()) {
        	TJob job = iterator.next();
        	System.out.println(new Gson().toJson( job ));
		}
	}
    
    /**
     * 分页查询全部列表
     */
	@Test
	public void pageSelectAll() {
		int pageNum = 1;
		int pageSize = 10;
        List<TJob> result = dsl.select().from(jb).offset((pageNum-1)*pageSize).limit(pageSize).fetchInto(TJob.class);
        Iterator<TJob> iterator = result.iterator();
        while(iterator.hasNext()) {
        	TJob job = iterator.next();
        	System.out.println(new Gson().toJson(job));
		}
	}
	
}
