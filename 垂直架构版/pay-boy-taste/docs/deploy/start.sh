#!/bin/bash

if [ ! -d backup ];then
  mkdir backup
fi

if [ ! -d logs ];then
  mkdir logs
fi

if [ ! -f payboy.jar ];then
  touch payboy.jar
fi

if [ ! -f payboy.log ];then
  touch payboy.log
fi

timeStr=`date +%Y%m%d_%H_%M_%S`

ps -ef |grep payboy |awk '{print $2}'|xargs kill -9
cp payboy.jar backup/payboy.$timeStr.jar
cp pay-boy-web-admin-0.0.1-SNAPSHOT.jar payboy.jar

mv payboy.log logs/payboy.log.$timeStr

nohup java -jar -Djava.security.egd=file:/dev/./urandom  -Dspring.config.location=./application.yml,./application-prod.yml payboy.jar >> payboy.log  2>&1 &

tail -f payboy.log