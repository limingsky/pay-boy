package com.ndood.admin.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.RolePo;
import com.ndood.admin.pojo.system.UserPo;
import com.ndood.admin.pojo.system.dto.RoleDto;
import com.ndood.admin.pojo.system.dto.UserDto;
import com.ndood.admin.pojo.system.query.UserQuery;
import com.ndood.admin.service.system.SystemRoleService;
import com.ndood.admin.service.system.SystemUserService;

/**
 * 用户控制器类
 * @author ndood
 */
@Controller
public class SystemUserController {
	@Autowired
	private SystemUserService systemUserService;
	
	@Autowired
	private SystemRoleService systemRoleService;
	
	/**
	 * 显示用户页
	 * 坑，如果GetMapping不写value，有可能访问不到静态资源
	 */
	@GetMapping("/system/user")
	public String toUserPage(){
		return "system/user/user_page";
	}
	
	/**
	 * 跳转到添加页
	 * @throws Exception 
	 */
	@GetMapping("/system/user/add")
	public String toAddUser(Model model) throws Exception{
		// Step1: 查询出所有角色
		List<RoleDto> roles = systemRoleService.getRoles();
		model.addAttribute("roles", roles);
		return "system/user/user_add";
	}
	
	/**
	 * 添加用户
	 * @throws Exception 
	 */
	@PostMapping("/system/user/add")
	@ResponseBody
	public AdminResultVo addUser(@RequestBody UserDto user) throws Exception{
		systemUserService.addUser(user);
		return AdminResultVo.ok().setMsg("添加用户成功！");
	}
	
	/**
	 * 删除用户
	 */
	@PostMapping("/system/user/delete")
	@ResponseBody
	public AdminResultVo deleteUser(Integer id){
		Integer[] ids = new Integer[]{id};
		systemUserService.batchDeleteUser(ids);
		return AdminResultVo.ok().setMsg("删除用户成功！");
	}

	/**
	 * 批量删除用户
	 */
	@PostMapping("/system/user/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeleteUser(@RequestParam("ids[]") Integer[] ids){
		systemUserService.batchDeleteUser(ids);
		return AdminResultVo.ok().setMsg("批量删除用户成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/user/update")
	public String toUpdateUser(Integer id, Model model) throws Exception{
		UserPo user = systemUserService.getUser(id);
		List<RoleDto> roles = systemRoleService.getRoles();
		for (RoleDto role : roles) {
			for (RolePo urole : user.getRoles()) {
				if(urole.getId()==role.getId()) {
					role.setIsInChecked(true);
					break;
				}
			}
		}
		model.addAttribute("roles", roles);
		model.addAttribute("user", user);
		return "system/user/user_update";
	}
	
	/**
	 * 修改用户
	 * @throws Exception 
	 */
	@PostMapping("/system/user/update")
	@ResponseBody
	public AdminResultVo updateUser(@RequestBody UserDto user) throws Exception{
		systemUserService.updateUser(user);
		return AdminResultVo.ok().setMsg("修改用户成功！");
	}
	
	/**
	 * 用户列表
	 * @throws Exception 
	 */
	@PostMapping("/system/user/query")
	@ResponseBody
	public DataTableDto getDectListPage(@RequestBody UserQuery query) throws Exception{
		DataTableDto page = systemUserService.pageUserList(query);
		return page;
	}
}
