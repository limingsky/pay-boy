package com.ndood.admin.core.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;

import com.ndood.admin.core.constaints.AdminConstaints;
import com.ndood.admin.pojo.system.UserPo;
import com.ndood.admin.repository.system.UserRepository;

/**
 * 如果配了，就静默注册一个connection，如果不配，就会抛出异常，跳转到SocialBeanConfig中配置的注册页上
 * 实现connectionSignUp，配置了该类SocialConfig中会设置signUp。
 * SocialAuthenticationFilter 333中会根据配置的signUp返回的唯一标识userId创建一个connection记录
 */
/*@Component*/
public class AdminConnectionSignUp implements ConnectionSignUp{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	/**
	 * 处理默默signUp逻辑
	 * 静默方式注册的时候根据socialUserId创建一个用户，登录的时候根据socialUserId鉴权
	 */
	@Override
	public String execute(Connection<?> connection) {
		// 创建一个用户
		String socialUserId = connection.getKey().toString();
		
		UserPo user = new UserPo();
		user.setNickName("匿名用户");
		user.setPassword(passwordEncoder.encode("123456"));
		user.setStatus(AdminConstaints.STATUS_OK);
		user.setHeadImgUrl(connection.getImageUrl());
		user.setCreateTime(new Date());
		user.setUpdateTime(new Date());
		user.setSocialUserId(socialUserId);
		user.setBirthday("2000-01-01");
		user.setSort(1);
		userRepository.save(user);
		
		// 根据社交用户信息默认创建用户，并返回用户唯一标识。
		return user.getUserId();
	}

}