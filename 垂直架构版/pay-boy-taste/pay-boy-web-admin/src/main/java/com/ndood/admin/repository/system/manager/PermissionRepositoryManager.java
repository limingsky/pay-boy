package com.ndood.admin.repository.system.manager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ndood.admin.pojo.system.dto.PermissionDto;

@SuppressWarnings("unchecked")
@Repository
public class PermissionRepositoryManager {
	
	@Autowired
	@PersistenceContext
	private EntityManager em;
	
	public List<PermissionDto> getPermissionList() {
		Query query = em.createQuery("SELECT new com.ndood.admin.pojo.system.dto.PermissionDto(p.id, p.name, p.desc, p.icon, p.sort, p.type, p.url, p.status, p.createTime, p.updateTime, pp.id as parentId) "
			+ "FROM PermissionPo p LEFT JOIN p.parent pp ON 1 = 1 "
			+ "WHERE 1 = 1 "
			+ "ORDER BY p.sort ASC ");
		
		query.setHint("org.hibernate.cacheable", true);
		List<PermissionDto> resultList = query.getResultList();
		return resultList;
	}
	
}
