package com.ndood.admin.service.system.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndood.admin.pojo.system.PermissionPo;
import com.ndood.admin.pojo.system.dto.PermissionDto;
import com.ndood.admin.repository.system.PermissionRepository;
import com.ndood.admin.repository.system.manager.PermissionRepositoryManager;
import com.ndood.admin.service.system.SystemPermissionService;
import com.ndood.core.utils.JPAUtil;

/**
 * 资源模块业务类
 * @author ndood
 */
@Transactional
@Service
public class SystemPermissionServiceImpl implements SystemPermissionService {
	
	@Autowired
	private PermissionRepository permissionDao;

	@Autowired
	private PermissionRepositoryManager permissionRepositoryManager;
	
	@Override
	public List<PermissionDto> getPermissionList() throws Exception {
		return permissionRepositoryManager.getPermissionList();
	}

	@Override
	public void batchDeletePermission(Integer[] ids) {
		for (Integer id : ids) {
			permissionDao.deleteById(id);
		}
	}

	@Override
	public PermissionDto addPermission(PermissionDto dto) throws Exception {
		// Step1: 补充相关字段
		dto.setCreateTime(new Date());
		dto.setUpdateTime(new Date());
		PermissionPo po = new PermissionPo();
		
		// Step2: 处理上级资源
		Integer parentId = dto.getParentId();
		if (parentId != null) {
			PermissionPo parent = permissionDao.findById(parentId).get();
			po.setParent(parent);
		}

		// Step3: 保存并返回
		JPAUtil.childToFather(dto, po);
		po = permissionDao.save(po);
		dto.setId(po.getId());
		return dto;
	}

	@Override
	public PermissionDto getPermission(Integer id) throws Exception {
		// Step1: 获取资源信息
		PermissionDto dto = new PermissionDto();
		PermissionPo po = permissionDao.findById(id).get();
		JPAUtil.fatherToChild(po, dto);
		dto.setParent(po.getParent());
		return dto;
	}

	@Override
	public PermissionDto updatePermission(PermissionDto dto) throws Exception {
		// Step1: 更新资源信息
		dto.setUpdateTime(new Date());
		PermissionPo po = permissionDao.findById(dto.getId()).get();
		JPAUtil.childToFather(dto, po);
		permissionDao.save(po);
		
		// Step2: 获取隐含属性用于异步回显
		JPAUtil.fatherToChild(po, dto);
		return dto;
	}
}
