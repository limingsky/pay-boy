package com.ndood.admin.controller.system;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.core.constaints.AdminErrCode;
import com.ndood.admin.core.exception.AdminException;
import com.ndood.admin.core.quartz.ScheduleJob;
import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.dto.JobDto;
import com.ndood.admin.pojo.system.query.JobQuery;
import com.ndood.admin.service.system.SystemJobService;
/**
 * 消息推送服务
 */
@Controller
public class SystemJobController {
	
	@Autowired
	private SystemJobService systemJobService;
	@GetMapping("/system/job")
	String toJobPage() {
		return "system/job/job_page";
	}
	/**
	 * 查询定时任务列表
	 */
	@ResponseBody
	@PostMapping("/system/job/query")
	public DataTableDto getJobListPage(@RequestBody JobQuery query) {
		DataTableDto page = systemJobService.pageUserList(query);
		return page;
	}
	/**
	 * 显示添加定时任务页面
	 */
	@GetMapping("/system/job/add")
	String toAddJob() {
		return "system/job/job_add";
	}
	/**
	 * 添加定时任务
	 */
	@PostMapping("/system/job/add")
	@ResponseBody
	public AdminResultVo addJob(@RequestBody JobDto job) throws Exception{
		systemJobService.addJob(job);
		return AdminResultVo.ok().setMsg("添加定时任务成功！");
	}
	
	/**
	 * 删除定时任务
	 */
	@PostMapping("/system/job/delete")
	@ResponseBody
	public AdminResultVo deleteJob(Integer id){
		Integer[] ids = new Integer[]{id};
		systemJobService.batchDeleteJob(ids);
		return AdminResultVo.ok().setMsg("删除定时任务成功！");
	}
	/**
	 * 批量删除任务
	 */
	@PostMapping("/system/job/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeleteJob(@RequestParam("ids[]") Integer[] ids){
		systemJobService.batchDeleteJob(ids);
		return AdminResultVo.ok().setMsg("批量删除定时任务成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/job/update")
	public String toUpdateJob(Integer id, Model model) throws Exception{
		JobDto job = systemJobService.getJob(id);
		model.addAttribute("job", job);
		return "system/job/job_update";
	}
	
	/**
	 * 修改定时任务
	 */
	@PostMapping("/system/job/update")
	@ResponseBody
	public AdminResultVo updateJob(@RequestBody JobDto job) throws Exception{
		systemJobService.updateJob(job);
		return AdminResultVo.ok().setMsg("修改定时任务成功！");
	}
	
	@PostMapping(value = "/system/job/change_status")
	@ResponseBody
	public AdminResultVo changeJobStatus(Integer id,String status) throws Exception {
		if(!ScheduleJob.STATUS_NOT_RUNNING.equals(status)&&!ScheduleJob.STATUS_RUNNING.equals(status)) {
			throw new AdminException(AdminErrCode.ERR_PARAM,"无效的任务状态！");
		}
		systemJobService.changeJobStatus(id, status);
		return AdminResultVo.ok().setMsg("更新定时任务状态成功！");
	}
}